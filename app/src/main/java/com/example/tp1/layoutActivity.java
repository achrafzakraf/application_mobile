package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;



public class layoutActivity extends AppCompatActivity {

    TextView nomAnimal;
    TextView poidNaissance;
    TextView espVie;
    TextView perGestation;
    TextView adultWeight;
    EditText conservStatut;
    ImageView imageAnimal;
    Button boutton ;





    protected  void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout);
        Intent intent = getIntent();
        String titre = intent.getStringExtra("animal");

        // recuperer les informations des animaux
        final Animal recupInfo=  AnimalList.getAnimal(titre);
        float recupBirthWeight=  recupInfo.getBirthWeight();
        int recupEspDeVie= recupInfo.getHightestLifespan();
        int recupPerGest=recupInfo.getGestationPeriod();
        int recupAdultWeight = recupInfo.getAdultWeight();
        String recupConservStatut = recupInfo.getConservationStatus();
        String recupImage = recupInfo.getImgFile();

        // Recupere l'idée du xml
        espVie= (TextView) findViewById(R.id.espVieAnimal);
        nomAnimal=(TextView) findViewById(R.id.titre);
        poidNaissance=(TextView) findViewById(R.id.poidsNaisAnimal);
        perGestation=(TextView) findViewById(R.id.perGestAnimal);
        adultWeight=(TextView) findViewById(R.id.poidsAdulteAnimal);
        conservStatut=(EditText) findViewById(R.id.statutConvAnimal) ;
        imageAnimal= findViewById(R.id.image) ;

        // Remplacer la valeur dans notre xml par les infos
        nomAnimal.setText(titre);
        poidNaissance.setText(""+ recupBirthWeight+"kgs");
        espVie.setText(""+ recupEspDeVie +"ans");
        perGestation.setText(""+recupPerGest+"jours");
        adultWeight.setText(""+recupAdultWeight+"kgs");
        conservStatut.setText(""+recupConservStatut);

        imageAnimal.setImageResource(this.getResources().getIdentifier(recupImage, "drawable",this.getPackageName()));


        boutton = (Button) findViewById(R.id.sauvegarder);
        boutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                recupInfo.setConservationStatus(conservStatut.getText().toString());
                Intent intent = new Intent(layoutActivity.this, MainActivity.class);
                startActivity(intent);
            }

        });




    }
}
