package com.example.tp1;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


class RowHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
    TextView label=null;
    ImageView icon=null;
    RowHolder(View row) {
        super(row);
//recuperer les id ou mettre le nom et l'icone de l'animal
        label=(TextView)row.findViewById(R.id.label);
        icon=(ImageView)row.findViewById(R.id.icon);
        this.itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){

        final String item = label.getText().toString();
        Intent intent = new Intent(this.itemView.getContext(), layoutActivity.class);
        intent.putExtra("animal", item);
        this.itemView.getContext().startActivity(intent);

    }
    void bindModel(String item) {
        label.setText(item);
        Animal animal = AnimalList.getAnimal(item);
        icon.setImageResource(this.itemView.getContext().getResources().getIdentifier(animal.getImgFile(), "drawable",this.itemView.getContext().getPackageName()));

    }


}